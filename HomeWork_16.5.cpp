#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

using namespace std;
int const N = 5;

int main()
{
	int A[N][N];
	time_t t;
	time(&t);
	int d = (localtime(&t)->tm_mday) % N;

	int Sum = 0;

	for (int i = 0; i < N; i++)
	{

		for (int j = 0; j < N; j++)
		{
			A[i][j] = i + j;
			cout << A[i][j] << ' ';
		}
		cout << "\n";
	}
	cout << "Sum of row " << d << ": ";
	for (int i = 0; i < N; i++)
	{
		Sum += A[d][i];
	}
	cout << Sum << "\n";
}
